Image Previewer

Drag and Drop: The image is dragged from the computer and dropped on the previewer.
Image Link: The image link is provided.

**Design Notes**

Some decisions were made intuitively. I didn't have accurate specification and I didn't have an idea of how much time do I 
need to spend on this task. 
I did not use any 3rd party libraries and compiler tools and css frameworks in that task. I wanted to demonstrate my skills 
with raw javascript. I've added some @todo to that place that were not clarified in specification. 
I'm planing to use babel, react, bootstrap on the next task.

**Demo**

https://youtu.be/0Vf2wEENVg0