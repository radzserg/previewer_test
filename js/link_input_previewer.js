(function () {
    /**
     * Drop previewer - allows to previewFromFile dropped files
     *
     * @param {HTMLElement} inputField
     * @param {ImagePreviewer} imagePreviewer
     * @constructor
     */
    var LinkInputPreviewer = function (inputField, imagePreviewer) {
        this.inputField = inputField;
        this.imagePreviewer = imagePreviewer;
        inputField.addEventListener('change', onValueChanged.bind(this));

        function onValueChanged(event) {
            var value = event.target.value;
            if (value.trim() === "") {
                imagePreviewer.hide();
                return;
            }

            if (!isValidUrl(value)) {
                alert('Please provide valid URL');
                return;
            }

            this.imagePreviewer.previewFromUrl(value);
            if (typeof this.onChange === 'function') {
                this.onChange();
            }
        }
    };

    LinkInputPreviewer.prototype.onChange = function(onChange) {
        this.onChange = onChange;
    };

    LinkInputPreviewer.prototype.clearInputValue = function () {
        this.inputField.value = '';
    };

    function isValidUrl(url) {
        var regex = new RegExp("(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})");
        return url.match(regex);
    }

    window.LinkInputPreviewer = LinkInputPreviewer;
})();