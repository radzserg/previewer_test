(function () {
    /**
     * Preview file in html element
     *
     * @param {HTMLElement} previewImage - element that will show previewFromFile
     * @param {string} unavailableImage - URL to fallback image
     * @constructor
     */
    var ImagePreviewer = function (previewImage, unavailableImage) {
        this.previewImage = previewImage;
        this.unavailableImage = unavailableImage;

        this.previewImage.addEventListener('error', function() {
            onSrcError(this);
        }.bind(this));
    };

    /**
     * Show default unavailable inage in case of onload error
     * @param {ImagePreviewer} imagePreviewer
     */
    function onSrcError(imagePreviewer) {
        imagePreviewer.previewImage.src = imagePreviewer.unavailableImage;
    }

    /**
     * Preview file in previewImage element
     * @param {File} file
     */
    ImagePreviewer.prototype.previewFromFile = function(file) {
        var previewImage = this.previewImage;
        previewImage.classList.remove('hide');

        var fileReader = new FileReader();
        if (file.type && file.type.search(/image/) !== -1) {
            fileReader.onloadend = function (event) {
                previewImage.src = event.target.result;
            };
            fileReader.readAsDataURL(file);
        } else {
            previewImage.src = this.unavailableImage;
        }
    };

    /**
     * Hides image
     */
    ImagePreviewer.prototype.hide = function() {
        this.previewImage.classList.add('hide');
    };

    /**
     * Preview image provided with URL
     * @param url
     */
    ImagePreviewer.prototype.previewFromUrl = function(url) {
        if (typeof url === "string" && url.trim() === "") {
            this.hide();
            return;
        }
        var previewImage = this.previewImage;
        previewImage.classList.remove('hide');
        previewImage.src = url;
    };

    window.ImagePreviewer = ImagePreviewer;
})();
