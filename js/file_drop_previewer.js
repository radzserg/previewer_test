(function () {
    /**
     * Drop previewer - allows to previewFromFile dropped files
     *
     * @param {HTMLElement} dropZone
     * @param {ImagePreviewer} imagePreviewer
     * @constructor
     */
    var DropPreviewer = function (dropZone, imagePreviewer) {
        this.imagePreviewer = imagePreviewer;

        var filePathHints = dropZone.querySelectorAll('.file-path-hint');
        this.filePathHint = filePathHints.length ? filePathHints[0] : null;

        ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
            dropZone.addEventListener(eventName, preventDefaults, false)
        });

        ['dragenter', 'dragover'].forEach(function (eventName) {
            dropZone.addEventListener(eventName, highlight, false)
        });
        ['dragleave', 'drop'].forEach(function (eventName) {
            dropZone.addEventListener(eventName, unhighlight, false)
        });

        dropZone.addEventListener('drop', (function(event) {
            handleDrop(this, event)
        }).bind(this), false);

        /**
         * Start highlight dropzone
         */
        function highlight() {
            dropZone.classList.add('highlight');
        }

        /**
         * Stop highlight dropzone
         */
        function unhighlight() {
            dropZone.classList.remove('highlight');
        }

        function preventDefaults(event) {
            event.preventDefault();
            event.stopPropagation();
        }
    };

    DropPreviewer.prototype.onChange = function(onChange) {
        this.onChange = onChange;
    };

    DropPreviewer.prototype.clearPathHint = function() {
        updateFileHint(this.filePathHint, null);
    };

    /**
     * Handle new file drop event
     * @param {DropPreviewer} dropPreviewer
     * @param {Event} event
     */
    function handleDrop(dropPreviewer, event) {
        var dataTransfer = event.dataTransfer;
        var files = dataTransfer.files;
        var file = selectFileToPreview(files);

        updateFileHint(dropPreviewer.filePathHint, file.name);
        dropPreviewer.imagePreviewer.previewFromFile(file);
        if (typeof dropPreviewer.onChange === 'function') {
            dropPreviewer.onChange();
        }

        /**
         * Defines what file we need to previewFromFile
         *
         * @todo: define what to do if multiple files are selected
         * @todo: define what to do if not an image file have been provided
         * @param files
         * @return {File}
         */
        function selectFileToPreview(files) {
            if (!files.length) {
                alert("Nothing to previewFromFile");
            }
            if (files.length > 1) {
                alert("Please select one file");
            }
            return files.item(0);
        }
    }

    /**
     * Updates file path hint
     * @param  {HTMLElement|null} filePathHint
     * @param {string|null} filePath
     */
    function updateFileHint(filePathHint, filePath) {
        if (!filePathHint) {
            return;
        }
        filePathHint.innerHTML = filePath;
        if (filePathHint) {
            filePathHint.classList.remove('hide');
        } else {
            filePathHint.classList.add('hide');
        }
    }

    window.DropPreviewer = DropPreviewer;
})();
