(function() {
    var previewer = new window.ImagePreviewer(document.getElementById('preview'), './images/preview-unavailable.jpg');

    var dropPreviewer = new window.DropPreviewer(document.getElementById('drop_zone'), previewer);
    var linkPreviewer = new window.LinkInputPreviewer(document.getElementById('link_previewer'), previewer);
    dropPreviewer.onChange = function() {
        linkPreviewer.clearInputValue();
    };
    linkPreviewer.onChange = function() {
        dropPreviewer.clearPathHint();
    };
})();
